async function getItems() {
  try {  
  const response = await fetch('http://195.181.210.249:3000/todo/');
  const parsedResponse = await response.json();  
  renderTodoList(parsedResponse);
  } catch(error) {
    console.error(error);
  }
}

async function createItem(text) {
  try {
  await fetch('http://195.181.210.249:3000/todo/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ title: text, extra: false }),    
  });
  await getItems();
  } catch(error) {
    console.error(error);
  }  
}

async function deleteItem(deletedKey) {
  try {
  await fetch('http://195.181.210.249:3000/todo/' + deletedKey, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    },    
  });
  await getItems()
  } catch {
    console.error(error);
  }
}

async function correctItem(correctedKey) {
  const index = parsedResponse.findIndex(item => item.id === Number(correctedKey));
  let responseText = prompt("Please correct your todo:", parsedResponse[index].text);
  if (responseText === null) {
    return;
    } else {
      try {
      await fetch('http://195.181.210.249:3000/todo/' + correctedKey, {
      method: 'PUT',
      headers: {
      'Content-Type': 'application/json',
      },
      body: JSON.stringify( { title: responseText }),
      });
      await getItems();           
      } catch {
      console.error(error);
      }  
    }
}

async function toggleDone(markAsDoneKey) {
  // const index = parsedResponse.findIndex(item => item.id === Number(markAsDoneKey));
  let value = parsedResponse[markAsDoneKey].extra;
  let reversedValue = !value;
  try {
    await fetch('http://195.181.210.249:3000/todo/' + markAsDoneKey, {
    method: 'PUT',
    headers: {
    'Content-Type': 'application/json',
    },
    body: JSON.stringify( { extra: reversedValue }),
    });
    await getItems();
    toggleDoneStyle(index); 
  } catch {
  console.error(error);
  }
}
