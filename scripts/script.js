//Elements of DOM

let responseText = "";
let deletedKey;
let correctedKey;
let revesedValueKey;
let parsedResponse = [];
let ulList = document.querySelector('.js-todo-ul');
let form = document.querySelector('.todo-form');
let todoText = document.querySelector('span');


function main() {
  eventsCreated(); 
  getItems();
}

//Events we use
function eventsCreated() {  
  ulList.addEventListener('click', event => {
    if (event.target.classList.contains('js-uncheck')) {
      const itemKey = event.target.parentElement.dataset.key;
      toggleDone(itemKey);
    }
    if (event.target.classList.contains('js-todo-delete')) {
      const deletedKey = event.target.parentElement.dataset.key;
      deleteItem(deletedKey);
    }
    if (event.target.tagName.toLowerCase() === 'span') {
      const correctedKey = event.target.parentElement.dataset.key;
      correctItem(correctedKey);
    }
  });  
}

form.addEventListener('submit', event => {
  event.preventDefault();
  const inputValue = document.querySelector('.js-todo-input');
  const txt = inputValue.value.trim();
  if (txt === '') {
    console.log('empty input');
    return;
  } else if (txt !== '') { //tu wysylanie POST
    createItem(txt);
    inputValue.value = '';
    inputValue.focus();
  }
});


//render todo items after adding new or correcting old ones
function renderTodoList(response) {
  ulList.innerHTML = "";
  parsedResponse = response;
  parsedResponse.forEach( function(element) {
    ulList.insertAdjacentHTML('beforeend', `
      <li class="todo-li" data-key="${element.id}">     
        <input id="${element.id}" type="checkbox"/>
        <label for="${element.id}" class="uncheck js-uncheck"></label>
          <span>${element.title}</span>
        <button class="todo-delete js-todo-delete">
          <i class="fas fa-trash-alt"></i>
        </button>
      </li>`);
  });    
}

function toggleDoneStyle(idx) {
  const item = document.querySelector(`[data-key='${idx}']`);
  if (parsedResponse[idx].checked) {
    item.classList.add('todo-done');
  } else {
    item.classList.remove('todo-done');
  }
}



document.addEventListener('DOMContentLoaded', main);
